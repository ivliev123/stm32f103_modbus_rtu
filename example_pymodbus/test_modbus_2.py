from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient
import time
import struct


# client = ModbusSerialClient(method="rtu", port="COM8", stopbits=1, bytesize=8, parity="N", baudrate=57600, timeout=0.2)
# print(client )

# connection = client.connect()
# connect_status = client.connect()
# print("Connect_status", connect_status)


class cleanbotics_V2:
    def __init__(self): 
        print("init")
        # rospy.init_node('RS_485_node')

        self.id_stm32_main_bord = 1
        self.id_brusher_bord = 2

        self.id_sonar_bord = 6

        self.time_brusher_up = time.time()
        self.flag_brusher_up = 1

        self.client = ModbusSerialClient(method="rtu", port="COM8", stopbits=1, bytesize=8, parity="N", baudrate=57600, timeout=0.2)
        print(self.client)

        connection = self.client.connect()
        connect_status = self.client.connect()
        print("Connect_status", connect_status)


    def uint_16_to_reg(self, addr, reg, data):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
        builder.add_16bit_int(data)
        registers = builder.to_registers()
        write_result = self.client.write_registers(reg, registers, unit=addr)

    def relay(self, relay_num, data):
        reg_num = relay_num + 27
        self.uint_16_to_reg(self.id_stm32_main_bord, reg_num, data)

 

    def ros_loop(self):
        # while not rospy.is_shutdown():
        while True:



            result = self.client.read_holding_registers(0, 10,  unit=1)
            print("main")
            print(result.registers)

            self.relay(8, 0)

            
            result = self.client.read_holding_registers(0, 10,  unit=2)
            print("brusher")
            print(result.registers)
            
            time.sleep(0.1)

            # self.relay_add(1,0)


            builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)       
            builder.add_32bit_int(-50)
            registers = builder.to_registers()
            write_result = self.client.write_registers(5, registers, unit=2)

            builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
            builder.add_16bit_uint(1)
            registers = builder.to_registers()
            write_result = self.client.write_registers(7, registers, unit=2)




if __name__ == '__main__':
    # try:
    if 1:
        bot = cleanbotics_V2()
        bot.ros_loop()

    # except:
    #     pass

    # except rospy.ROSInterruptException:
    #     pass