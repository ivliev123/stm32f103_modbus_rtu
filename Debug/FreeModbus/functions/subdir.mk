################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeModbus/functions/mbfunccoils.c \
../Middlewares/FreeModbus/functions/mbfunccoils_m.c \
../Middlewares/FreeModbus/functions/mbfuncdiag.c \
../Middlewares/FreeModbus/functions/mbfuncdisc.c \
../Middlewares/FreeModbus/functions/mbfuncdisc_m.c \
../Middlewares/FreeModbus/functions/mbfuncholding.c \
../Middlewares/FreeModbus/functions/mbfuncholding_m.c \
../Middlewares/FreeModbus/functions/mbfuncinput.c \
../Middlewares/FreeModbus/functions/mbfuncinput_m.c \
../Middlewares/FreeModbus/functions/mbfuncother.c \
../Middlewares/FreeModbus/functions/mbutils.c 

OBJS += \
./FreeModbus/functions/mbfunccoils.o \
./FreeModbus/functions/mbfunccoils_m.o \
./FreeModbus/functions/mbfuncdiag.o \
./FreeModbus/functions/mbfuncdisc.o \
./FreeModbus/functions/mbfuncdisc_m.o \
./FreeModbus/functions/mbfuncholding.o \
./FreeModbus/functions/mbfuncholding_m.o \
./FreeModbus/functions/mbfuncinput.o \
./FreeModbus/functions/mbfuncinput_m.o \
./FreeModbus/functions/mbfuncother.o \
./FreeModbus/functions/mbutils.o 

C_DEPS += \
./FreeModbus/functions/mbfunccoils.d \
./FreeModbus/functions/mbfunccoils_m.d \
./FreeModbus/functions/mbfuncdiag.d \
./FreeModbus/functions/mbfuncdisc.d \
./FreeModbus/functions/mbfuncdisc_m.d \
./FreeModbus/functions/mbfuncholding.d \
./FreeModbus/functions/mbfuncholding_m.d \
./FreeModbus/functions/mbfuncinput.d \
./FreeModbus/functions/mbfuncinput_m.d \
./FreeModbus/functions/mbfuncother.d \
./FreeModbus/functions/mbutils.d 


# Each subdirectory must supply rules for building sources it contributes
FreeModbus/functions/mbfunccoils.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfunccoils.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfunccoils_m.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfunccoils_m.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncdiag.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncdiag.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncdisc.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncdisc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncdisc_m.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncdisc_m.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncholding.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncholding.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncholding_m.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncholding_m.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncinput.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncinput.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncinput_m.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncinput_m.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbfuncother.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbfuncother.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

FreeModbus/functions/mbutils.o: C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions/mbutils.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/test_modbus_stm32f103/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


